Source: enigmail
Section: mail
Priority: optional
Maintainer: Debian Mozilla Extension Maintainers <pkg-mozext-maintainers@lists.alioth.debian.org>
Uploaders:
 Alexander Sack <asac@debian.org>,
 Willi Mann <willi@debian.org>,
 Daniel Kahn Gillmor <dkg@fifthhorseman.net>,
Build-Depends:
 debhelper-compat (= 12),
 mozilla-devscripts,
 perl,
 python,
 zip,
Standards-Version: 4.4.0
Homepage: https://www.enigmail.net/
Vcs-Git: https://salsa.debian.org/debian/enigmail.git -b debian/master
Vcs-Browser: https://salsa.debian.org/debian/enigmail
Rules-Requires-Root: no

Package: enigmail
Architecture: all
Depends:
 gnupg (>= 2.2.8-2~),
 gnupg-agent,
 ${misc:Depends},
 ${shlibs:Depends},
 ${xpi:Depends},
Recommends:
 pinentry-x11,
 ${xpi:Recommends},
Provides:
 ${xpi:Provides},
Enhances:
 ${xpi:Enhances},
Breaks:
 ${xpi:Breaks},
Description: GPG support for Thunderbird and Debian Icedove
 OpenPGP extension for Thunderbird. Enigmail allows users to access the
 features provided by the popular GnuPG software from within Thunderbird.
 .
 Enigmail is capable of signing, authenticating, encrypting and decrypting
 email. Additionally, it supports both the inline PGP format, as well as the
 PGP/MIME format as described in RFC 3156.
